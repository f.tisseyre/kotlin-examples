Password checker
===

Rules :
- At least 8 characters
- Should contain digits 
- Should contain one letter uppercase
- Should contain one letter lowercase
- Should contain one special character
- No word from dictionary (case insensitive)
- Not the same as the last 5