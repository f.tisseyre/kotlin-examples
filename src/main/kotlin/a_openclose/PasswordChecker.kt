package a_openclose

class PasswordChecker(private val rules: List<Rule>) {

    fun check(passwordToCheck: String): List<String> {
        return rules.filter { !it.check(passwordToCheck) }.map { it.display() }
    }
}
