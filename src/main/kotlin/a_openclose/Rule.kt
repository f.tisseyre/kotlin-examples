package a_openclose

interface Rule {
    fun check(passwordToCheck: String): Boolean

    fun display() : String
}
