package a_openclose.rules

import a_openclose.Rule

class DigitRule: Rule {
    override fun check(passwordToCheck: String): Boolean {
        return passwordToCheck.contains(Regex("[0-9]"))
    }

    override fun display(): String {
        return "Password should have at least one digit"
    }

}
