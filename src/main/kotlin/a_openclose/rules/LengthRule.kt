package a_openclose.rules

import a_openclose.Rule

class LengthRule: Rule {
    override fun check(passwordToCheck: String): Boolean {
        val maxLength = 8
        return passwordToCheck.length >= maxLength
    }

    override fun display(): String {
        return "Password should have at least 8 characters"
    }

}
