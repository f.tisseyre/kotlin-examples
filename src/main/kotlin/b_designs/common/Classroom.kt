package b_designs.common

data class Classroom(val name: String, val teacher: String, val students: List<Student>)
