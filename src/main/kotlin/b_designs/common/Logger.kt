package b_designs.common

interface Logger {
    fun print(message: String)
}
