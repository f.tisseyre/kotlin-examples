package b_designs.common

data class Student(val name: String, val surname: String, val averageScore: Float)
