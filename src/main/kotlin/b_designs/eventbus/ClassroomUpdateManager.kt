package b_designs.eventbus

import b_designs.common.Classroom
import b_designs.eventbus.handlers.ClassroomCreated
import b_designs.eventbus.handlers.NewStudentAdded
import b_designs.eventbus.handlers.TeacherChanged

class ClassroomUpdateManager(private val eventBus: EventBus) {
    var classroom: Classroom? = null

    fun receive(newClassroom: Classroom) {
        if (this.classroom == null) {
            eventBus.publish(ClassroomCreated())
        } else {
            if (classroom!!.teacher != newClassroom.teacher)
                eventBus.publish(TeacherChanged(newClassroom.teacher))

            newClassroom.students.subtract(classroom!!.students).forEach {
                eventBus.publish(NewStudentAdded(it))
            }
        }
        classroom = newClassroom
    }
}
