package b_designs.eventbus

import kotlin.reflect.KClass


class EventBus {
    val handlers: MutableMap<KClass<*>, Handler<Any>> = mutableMapOf()

    inline fun <reified T> subscribe(handler: Handler<T>) {
        handlers[T::class] = handler as Handler<Any>
    }

    inline fun <reified T> publish(event: T) {
        handlers[T::class]?.handle(event as Any)
    }
}
