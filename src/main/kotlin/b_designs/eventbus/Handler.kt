package b_designs.eventbus

interface Handler<in T> {
    fun handle(event: T)
}
