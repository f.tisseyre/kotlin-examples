package b_designs.eventbus

data class Data (val firstname: String)

interface Callee {
    fun call(data: Data): String
}

class Real : Callee {
    override fun call(data: Data): String {
        println("Real called")
        return "hello " + data.firstname
    }
}

class MiddleWare(val number: Int, val callee: Callee) : Callee {
    override fun call(data: Data): String {
        println("Middleware $number called")
        val result = callee.call(data)
        println("After middleware $number called")
        return result
    }
}

class Manager(val real : Callee) {
    fun apply() {
        real.call(Data("toto"))
    }
}

fun main() {
    val callee = MiddleWare(1, MiddleWare(2, MiddleWare(3, Real())))
    Manager(callee).apply()
}
