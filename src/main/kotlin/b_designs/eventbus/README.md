Event bus pattern
===

Pros
---
- Decouple change detection from change handling
- Change detection logic is in the same place (manager)
- soc respected

Cons
---
- ocp not respected : Manager should be changed for every new change detection
