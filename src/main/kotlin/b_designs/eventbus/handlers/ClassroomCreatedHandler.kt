package b_designs.eventbus.handlers

import b_designs.common.Logger
import b_designs.eventbus.Handler

class ClassroomCreated()

class ClassroomCreatedHandler(val logger: Logger) : Handler<ClassroomCreated> {
    override fun handle(event: ClassroomCreated) {
        logger.print("First classroom received")
    }
}
