package b_designs.eventbus.handlers

import b_designs.common.Logger
import b_designs.common.Student
import b_designs.eventbus.Handler

data class NewStudentAdded(val student: Student)

class NewStudentAddedHandler(private val logger: Logger) : Handler<NewStudentAdded> {
    override fun handle(event: NewStudentAdded) {
        logger.print("New student added: ${event.student.name} ${event.student.surname}")
    }
}
