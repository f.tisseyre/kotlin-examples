package b_designs.eventbus.handlers

import b_designs.common.Logger
import b_designs.eventbus.Handler

data class TeacherChanged(val name: String)

class TeacherChangedHandler(private val logger: Logger) : Handler<TeacherChanged> {
    override fun handle(event: TeacherChanged) {
        logger.print("Teacher changed for ${event.name}")
    }
}
