package b_designs.observer

import b_designs.common.Classroom

class ClassroomSubject(val observers : List<Observer>) {
    fun notifyObservers(classroom: Classroom) {
        observers.forEach { it.update(classroom) }
    }
}
