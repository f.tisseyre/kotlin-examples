package b_designs.observer

import b_designs.common.Classroom
import b_designs.common.Logger

class ClassroomUpdateManager(val logger: Logger, val subject: ClassroomSubject) {

    fun receive(newClassroom: Classroom) {
        subject.notifyObservers(newClassroom)
    }
}
