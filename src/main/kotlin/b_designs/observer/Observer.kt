package b_designs.observer

import b_designs.common.Classroom

interface Observer {
    fun update(classroom: Classroom)

}
