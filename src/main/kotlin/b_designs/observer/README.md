Subscriber patter
===

Pros
---
- Matches kotlin philosophy
- ocp respected : no need to change the manager for new detections


Cons
---
- Update isn't generic (classroom as parameter) -> prob if project bigger
- Every observer has to know whole classroom structure