package b_designs.observer.observers

import b_designs.common.Classroom
import b_designs.common.Logger
import b_designs.observer.Observer

class ClassroomInitializeObserver(val logger: Logger) : Observer {
    var firstCall = true
    override fun update(classroom: Classroom) {
        if (firstCall) {
            logger.print("First classroom received")
        }
        firstCall = false
    }
}
