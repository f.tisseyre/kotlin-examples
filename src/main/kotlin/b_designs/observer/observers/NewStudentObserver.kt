package b_designs.observer.observers

import b_designs.common.Classroom
import b_designs.common.Logger
import b_designs.common.Student
import b_designs.observer.Observer

class NewStudentObserver(val logger: Logger) : Observer{
    private var students = listOf<Student>()

    override fun update(newClassroom: Classroom) {
        newClassroom.students.subtract(students).forEach {
            logger.print("New student added: ${it.name} ${it.surname}")
        }
        students = newClassroom.students
    }
}
