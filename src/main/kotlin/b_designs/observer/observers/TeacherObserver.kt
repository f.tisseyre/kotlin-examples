package b_designs.observer.observers

import b_designs.common.Classroom
import b_designs.common.Logger
import b_designs.observer.Observer

class TeacherObserver(val logger: Logger) : Observer {
    private var teacher: String = ""

    override fun update(newClassroom: Classroom) {
        if (teacher != newClassroom.teacher) {
            logger.print("Teacher changed for ${newClassroom.teacher}")
        }
        teacher = newClassroom.teacher
    }
}
