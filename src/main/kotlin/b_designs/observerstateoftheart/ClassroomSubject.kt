package b_designs.observerstateoftheart

import b_designs.common.Classroom

class ClassroomSubject(val classroomUpdateManager: ClassroomUpdateManager) : Subject {
    var classroom: Classroom? = null
    private val observers: MutableList<Observer> = mutableListOf()

    override fun registerObserver(observer: Observer) {
        observers += observer
    }

    override fun notifyObservers() {
        this.classroom = classroomUpdateManager.classroom
        observers.forEach { it.update() }
    }
}
