package b_designs.observerstateoftheart

import b_designs.common.Classroom

class ClassroomUpdateManager() {
    lateinit var subject: ClassroomSubject
    var classroom: Classroom? = null

    fun registerSubject(subject: ClassroomSubject) {
        this.subject = subject
    }

    fun receive(newClassroom: Classroom) {
        classroom = newClassroom
        subject.notifyObservers()
    }
}
