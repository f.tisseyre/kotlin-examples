package b_designs.observerstateoftheart

interface Observer {
    fun update()
}
