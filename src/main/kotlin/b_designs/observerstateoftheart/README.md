Match exactly to state of the art
===

Pros 
---
- Do what you want in notifyObservers and update
- Don't depend on parameters

Cons
---
- Coupling between Manager and Subject
    - Less flexible
    - Building more complex
    - Intermediate state prone to error
    - Several usages of Nullable and safe call
