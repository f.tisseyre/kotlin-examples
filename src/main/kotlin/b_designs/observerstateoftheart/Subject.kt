package b_designs.observerstateoftheart

interface Subject {
    fun registerObserver(observer: Observer)
    fun notifyObservers()
}
