package b_designs.observerstateoftheart.observers

import b_designs.common.Logger
import b_designs.observerstateoftheart.Observer

class ClassroomInitializeObserver(val logger: Logger) : Observer {
    var firstCall = true

    override fun update() {
        if (firstCall) {
            logger.print("First classroom received")
        }
        firstCall = false
    }
}
