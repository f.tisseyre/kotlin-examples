package b_designs.observerstateoftheart.observers

import b_designs.common.Logger
import b_designs.common.Student
import b_designs.observerstateoftheart.ClassroomSubject
import b_designs.observerstateoftheart.Observer

class NewStudentObserver(val subject: ClassroomSubject, val logger: Logger) : Observer{
    private var students = listOf<Student>()

    override fun update() {
        subject.classroom!!.students.subtract(students).forEach {
            logger.print("New student added: ${it.name} ${it.surname}")
        }
        students = subject.classroom!!.students
    }
}
