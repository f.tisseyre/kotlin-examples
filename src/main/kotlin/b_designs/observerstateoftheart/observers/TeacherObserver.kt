package b_designs.observerstateoftheart.observers

import b_designs.common.Logger
import b_designs.observerstateoftheart.ClassroomSubject
import b_designs.observerstateoftheart.Observer

class TeacherObserver(val subject: ClassroomSubject, val logger: Logger) : Observer {
    private var teacher: String = ""

    override fun update() {
        val classroom = subject.classroom!!

        if (teacher != classroom.teacher) {
            logger.print("Teacher changed for ${classroom.teacher}")
        }
        teacher = classroom.teacher
    }
}
