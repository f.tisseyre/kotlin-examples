package b_designs.soc

import b_designs.common.Classroom
import b_designs.common.Logger

class ClassroomUpdateManager(private val logger: Logger) {
    var classroom: Classroom? = null

    fun receive(newClassroom: Classroom) {
        if (this.classroom == null) {
            logger.print("First classroom received")
        } else {
            if (classroom!!.teacher != newClassroom.teacher)
                logger.print("Teacher changed for ${newClassroom.teacher}")

            newClassroom.students.subtract(classroom!!.students).forEach {
                logger.print("New student added: ${it.name} ${it.surname}")
            }
        }
        classroom = newClassroom
    }
}
