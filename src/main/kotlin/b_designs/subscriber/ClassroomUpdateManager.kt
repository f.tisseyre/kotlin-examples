package b_designs.subscriber

import b_designs.common.Classroom
import b_designs.subscriber.subscribers.ClassroomCreated
import b_designs.subscriber.subscribers.NewStudentAdded
import b_designs.subscriber.subscribers.TeacherChanged
import kotlin.reflect.KClass

class ClassroomUpdateManager {

    val subscribers: MutableMap<KClass<*>, Subscriber<Any>> = mutableMapOf()

    inline fun <reified T> subscribe(subscriber: Subscriber<T>) {
        subscribers[T::class] = subscriber as Subscriber<Any>
    }

    inline fun <reified T> publish(event: T) {
        subscribers[T::class]?.handle(event as Any)
    }



    var classroom: Classroom? = null

    fun receive(newClassroom: Classroom) {
        if (this.classroom == null) {
            publish(ClassroomCreated())
        } else {
            if (classroom!!.teacher != newClassroom.teacher)
                publish(TeacherChanged(newClassroom.teacher))

            newClassroom.students.subtract(classroom!!.students).forEach {
                publish(NewStudentAdded(it))
            }
        }
        classroom = newClassroom
    }
}
