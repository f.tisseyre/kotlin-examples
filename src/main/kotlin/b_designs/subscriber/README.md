Subscriber patter
===

Pros
---
- Decouple change detection from change handling
- Change detection logic is in the same place (manager)

Cons
---
- ocp not respected : Manager should be changed for every new change detection
- soc not respected : Manager has the detection logic as well as detection of subscribers
