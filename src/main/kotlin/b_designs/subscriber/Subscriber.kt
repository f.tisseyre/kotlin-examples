package b_designs.subscriber

interface Subscriber<in T> {
    fun handle(event: T)
}
