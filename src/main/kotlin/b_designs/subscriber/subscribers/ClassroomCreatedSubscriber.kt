package b_designs.subscriber.subscribers

import b_designs.common.Logger
import b_designs.subscriber.Subscriber

class ClassroomCreated()

class ClassroomCreatedSubscriber(val logger: Logger) : Subscriber<ClassroomCreated> {
    override fun handle(event: ClassroomCreated) {
        logger.print("First classroom received")
    }
}
