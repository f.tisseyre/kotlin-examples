package b_designs.subscriber.subscribers

import b_designs.common.Logger
import b_designs.common.Student
import b_designs.subscriber.Subscriber

data class NewStudentAdded(val student: Student)

class NewStudentAddedSubscriber(private val logger: Logger) : Subscriber<NewStudentAdded> {
    override fun handle(event: NewStudentAdded) {
        logger.print("New student added: ${event.student.name} ${event.student.surname}")
    }
}
