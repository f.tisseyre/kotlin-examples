package b_designs.subscriber.subscribers

import b_designs.common.Logger
import b_designs.subscriber.Subscriber

data class TeacherChanged(val name: String)

class TeacherChangedSubscriber(private val logger: Logger) : Subscriber<TeacherChanged> {
    override fun handle(event: TeacherChanged) {
        logger.print("Teacher changed for ${event.name}")
    }
}
