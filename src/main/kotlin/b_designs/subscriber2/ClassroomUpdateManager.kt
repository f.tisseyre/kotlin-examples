package b_designs.subscriber2

import b_designs.common.Classroom
import b_designs.common.Logger
import kotlin.reflect.KClass

class ClassroomUpdateManager(private val logger: Logger) {
    var classroom: Classroom? = null
    var subscribers : MutableMap<KClass<*>, Subscriber<Any>> = mutableMapOf()

    inline fun <reified T> register(subscriber: Subscriber<T>) {
        subscribers[T::class] = subscriber as Subscriber<Any>
    }

    private inline fun <reified T> publish(event: T) {
        subscribers[T::class]?.receive(event as Any)
    }

    fun receive(newClassroom: Classroom) {
        if (this.classroom == null) {
            publish(FirstClassroomReceived(newClassroom))
        } else {
            if (classroom!!.teacher != newClassroom.teacher)
                logger.print("Teacher changed for ${newClassroom.teacher}")

            newClassroom.students.subtract(classroom!!.students).forEach {
                logger.print("New student added: ${it.name} ${it.surname}")
            }
        }
        classroom = newClassroom
    }
}
