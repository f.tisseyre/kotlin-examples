package b_designs.subscriber2

import b_designs.common.Classroom

data class FirstClassroomReceived(val newClassroom: Classroom)
