package b_designs.subscriber2

import b_designs.common.Logger

class NewClassroomSubscriber(val logger: Logger): Subscriber<FirstClassroomReceived> {
    override fun receive(event: FirstClassroomReceived) {
        logger.print("First classroom received")
    }

}
