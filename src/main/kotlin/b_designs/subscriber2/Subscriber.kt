package b_designs.subscriber2

interface Subscriber<T> {
    fun receive(event: T)
}
