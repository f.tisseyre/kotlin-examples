Avoid null
===

Kotlin discourage usage of null.
Null is considered as the billion dollar mistake in IT.
How to avoid use of it ?

Uses cases
---

- Produce null (return null)
    - Data could be present of absent depending on the context, missing data
- Get a null
    - Method operate on object that has not been completely initialized
    - At init stage
