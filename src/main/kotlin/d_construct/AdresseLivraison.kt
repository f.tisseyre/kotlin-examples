package d_construct

data class AdresseLivraison(
    private val numeroDeRue: Int,
    private val typeDeRue: TypeDeRue,
    private val nomDeRue: String
) {
 companion object {
     fun from(adresseSousFormeDeChaine: String): AdresseLivraison {
         val mots: List<String> = adresseSousFormeDeChaine.split(" ")
         return AdresseLivraison(mots[0].toInt(), TypeDeRue.valueOf(mots[1]), mots.slice(2..(mots.size-1)).joinToString(" "))
     }
 }
}

