package e_cqrs.cqrs

interface CompanyRepository {
    fun save(department: Department)
    fun read(departmentName: String): Department?
}