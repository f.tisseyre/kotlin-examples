package e_cqrs.cqrs

data class CreateDepartmentCommand(val name : String)