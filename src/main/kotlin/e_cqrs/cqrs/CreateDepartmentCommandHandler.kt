package e_cqrs.cqrs

class CreateDepartmentCommandHandler(val repository: CompanyRepository){

    fun handle(command : CreateDepartmentCommand){
        if (command.name.length < 2) throw InvalidDepartmentName()
        repository.save(Department(command.name, listOf()))
    }

}