package e_cqrs.cqrs

class InMemoryCompanyRepository : CompanyRepository {
    val values = mutableMapOf<String, Department>()

    override fun save(department: Department) {
        values[department.name] = department
    }

    override fun read(departmentName: String): Department? {
        return values[departmentName]
    }
}