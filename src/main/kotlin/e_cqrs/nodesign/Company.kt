package e_cqrs.nodesign

data class Company (val departments: List<Department>)

data class Department(val name: String, val employees: List<Employee>, val contractors: List<Contractor> = listOf())

open class Employee(val name: String, val contract: Contract = Contract.FullTime)

class Contractor(name: String, contract: Contract = Contract.FullTime) : Employee(name, contract)

enum class Contract {
    FullTime,
    HalfTime,
    EightyPercent
}
