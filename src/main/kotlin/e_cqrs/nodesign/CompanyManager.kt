package e_cqrs.nodesign

import kotlin.math.roundToInt

class CompanyManager(val company: Company) {
    fun countAccountability(departmentName: String): Int {
        val department = company.departments.first { it.name == departmentName }
        return department.employees.size + department.contractors.size
    }

    fun countHr(departmentName: String): Int {
        val department = company.departments.first { it.name == departmentName }
        return department.employees.size
    }

    fun countManager(departmentName: String): Int {
        val department = company.departments.first { it.name == departmentName }
        return (department.employees.sumByDouble { contractToDouble(it) } + department.contractors.sumByDouble { contractToDouble(it) }
                ).roundToInt()
    }

    private fun contractToDouble(it: Employee): Double = when (it.contract) {
        Contract.FullTime -> 1.0
        Contract.HalfTime -> 0.5
        Contract.EightyPercent -> 0.8
    }
}
