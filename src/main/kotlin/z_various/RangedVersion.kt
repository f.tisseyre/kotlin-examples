package z_various

data class RangedVersion(val major: Int, val minor: Int) {
    operator fun rangeTo(other: RangedVersion): List<RangedVersion> {
        if (major == other.major) return (minor..other.minor).map { RangedVersion(major, it) }
        if (major <= other.major) {
            val maxMinor = 100
            val first = this..RangedVersion(major, maxMinor)
            val middle = (major + 1 until other.major).map { RangedVersion(it, 0)..RangedVersion(it, maxMinor) }.flatten()
            val last = RangedVersion(other.major, 0)..other
            return first + middle + last
        }
        return listOf()
    }
}


