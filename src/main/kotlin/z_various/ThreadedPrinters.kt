package z_various

import kotlin.random.Random


fun print(docName: String, count: Int, waitOnThree: Thread?) {
    for(i in 1..count) {
        if (i == 3 && waitOnThree !== null) waitOnThree.join()
        println("$docName - $i / $count")
        Thread.sleep(Random(1).nextInt(100).toLong())
    }
}

open class ParentPrinter

class PrinterA(val docName: String, val count: Int, val waitOnThree: Thread? = null) : ParentPrinter(), Runnable {
    override fun run() {
        print(docName, count, waitOnThree)
    }
}

class PrinterManager() : Thread() {
    var threadA: Thread? = null
    var threadB: Thread? = null

    override fun run() {
        threadA = Thread(PrinterA("toto", 12))
        threadB = Thread(PrinterA("tata", 5))
        threadA?.start()
        threadB?.start()
    }
}
