package a_openclose

import io.kotest.matchers.collections.shouldBeEmpty
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class PasswordCheckerTest {
    lateinit var alwaysFalseRule : Rule
    lateinit var alwaysTrueRule : Rule

    @BeforeEach
    fun beforeEach() {
        alwaysFalseRule = object : Rule {
            override fun check(passwordToCheck: String): Boolean {
                return false;
            }

            override fun display(): String {
                return "Always false"
            }
        }
        alwaysTrueRule = object : Rule {
            override fun check(passwordToCheck: String): Boolean {
                return true;
            }

            override fun display(): String {
                return "Always true"
            }
        }
    }

    @Test
    fun `should reject if any rule is false`() {
        val passwordChecker = PasswordChecker(listOf(alwaysTrueRule, alwaysFalseRule, alwaysTrueRule))

        passwordChecker.check("dummy") shouldBe listOf("Always false")
    }

    @Test
    fun `should accept if all rules are true`() {
        val passwordChecker = PasswordChecker(listOf(alwaysTrueRule, alwaysTrueRule))

        passwordChecker.check("dummy").shouldBeEmpty()
    }
}
