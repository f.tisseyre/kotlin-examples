package a_openclose.rules

import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.booleans.shouldBeTrue
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test

internal class DigitRuleTest {
    @Test
    fun `should reject if no digits`() {
        DigitRule().check("AB").shouldBeFalse()
    }

    @Test
    fun `should accept if contains one digit`() {
        DigitRule().check("A1B").shouldBeTrue()
    }

    @Test
    fun `should display correctly in HMI`() {
        DigitRule().display() shouldBe "Password should have at least one digit"
    }
}
