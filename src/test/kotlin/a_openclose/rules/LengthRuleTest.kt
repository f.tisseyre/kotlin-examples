package a_openclose.rules

import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.booleans.shouldBeTrue
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test

internal class LengthRuleTest {
    @Test
    fun `should reject if password shorter than 8 char`() {
        LengthRule().check("1234567").shouldBeFalse()
    }

    @Test
    fun `should accept if password longer or equal to 8 char`() {
        LengthRule().check("12345678").shouldBeTrue()
    }

    @Test
    fun `should display correctly`() {
        LengthRule().display() shouldBe "Password should have at least 8 characters"
    }
}
