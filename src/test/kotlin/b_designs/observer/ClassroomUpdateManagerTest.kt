package b_designs.observer

import b_designs.common.Classroom
import b_designs.common.Logger
import b_designs.common.Student
import b_designs.observer.observers.ClassroomInitializeObserver
import b_designs.observer.observers.NewStudentObserver
import b_designs.observer.observers.TeacherObserver
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class ClassroomUpdateManagerTest {
    lateinit var messages: List<String>
    lateinit var logger: Logger

    @BeforeEach
    fun beforeEach() {
        messages = listOf<String>()
        logger = object : Logger {
            override fun print(message: String) {
                messages += message
            }
        }
    }

    @Test
    fun `should print "first classroom received" the first time`() {
        val classroom = Classroom("6A", "Martine", listOf())
        val subject = ClassroomSubject(listOf(ClassroomInitializeObserver(logger)))
        val sut = ClassroomUpdateManager(logger, subject)
        sut.receive(classroom)
        messages shouldBe listOf("First classroom received")
    }

    @Test
    fun `should detect change in teacher`() {
        val classroom = Classroom("6A", "Martine", listOf())
        val classroom2 = Classroom("6A", "Paul", listOf())
        val subject = ClassroomSubject(listOf(TeacherObserver(logger)))
        val sut = ClassroomUpdateManager(logger, subject)
        sut.receive(classroom)
        messages = listOf()
        sut.receive(classroom2)
        messages shouldBe listOf("Teacher changed for Paul")
    }

    @Test
    fun `should detect new student added`() {
        val classroom = Classroom("6A", "Martine", listOf())
        val classroom2 = Classroom("6A", "Martine", listOf(Student("Bill", "Palmer", 10f)))
        val subject = ClassroomSubject(listOf(NewStudentObserver(logger)))
        val sut = ClassroomUpdateManager(logger, subject)
        sut.receive(classroom)
        messages = listOf()
        sut.receive(classroom2)
        messages shouldBe listOf("New student added: Bill Palmer")
    }

    @Test
    fun `should detect a second student added`() {
        val classroom = Classroom("6A", "Martine", listOf(
            Student("Bill", "Palmer", 10f),
            Student("Bob", "Smith", 16f)
        ))
        val classroom2 = Classroom("6A", "Martine", listOf(
            Student("Bill", "Palmer", 10f),
            Student("Samantha", "Clinton", 3f),
            Student("Bob", "Smith", 16f)
        ))
        val subject = ClassroomSubject(listOf(NewStudentObserver(logger)))
        val sut = ClassroomUpdateManager(logger, subject)
        sut.receive(classroom)
        messages = listOf()
        sut.receive(classroom2)
        messages shouldBe listOf("New student added: Samantha Clinton")
    }
}
