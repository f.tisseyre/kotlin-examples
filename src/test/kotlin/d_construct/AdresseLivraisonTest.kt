package d_construct

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test

class AdresseLivraisonTest {
    @Test
    fun `doit transformer une adresse`() {
        val actual = AdresseLivraison.from("3 Boulevard des sabliers")
        val expected = AdresseLivraison(3, TypeDeRue.Boulevard, "des sabliers")
        actual shouldBe expected
    }
}
