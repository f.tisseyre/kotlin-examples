package e_cqrs.cqrs

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.nulls.shouldNotBeNull
import org.junit.jupiter.api.Test

class CreateDepartmentCommandHandlerTest {

    @Test
    fun `should allow department creation`() {
        val companyRepository = InMemoryCompanyRepository()
        val sut = CreateDepartmentCommandHandler(companyRepository)

        sut.handle(CreateDepartmentCommand("dep"))

        companyRepository.read("dep").shouldNotBeNull()
    }

    @Test
    fun `should deny department creation if the name has less than 2 chars`() {
        shouldThrow<InvalidDepartmentName> {
            CreateDepartmentCommandHandler(InMemoryCompanyRepository()).handle(CreateDepartmentCommand("a"))
        }
    }
}