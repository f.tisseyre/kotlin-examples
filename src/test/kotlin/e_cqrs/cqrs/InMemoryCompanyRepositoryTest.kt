package e_cqrs.cqrs

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test

class InMemoryCompanyRepositoryTest {

    @Test
    fun `should read saved department`(){
        val sut = InMemoryCompanyRepository()
        val department = Department("dep", listOf())

        sut.save(department)

        sut.read("dep") shouldBe department
    }
}