package e_cqrs.nodesign

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class CompanyManagerTest {
    lateinit var company: Company

    @BeforeEach
    fun init() {
        val accountability = Department("accountability", listOf(
            Employee("bob", Contract.HalfTime),
            Employee("bill", Contract.HalfTime),
            Employee("sarah")
        ))
        val hr = Department("hr",
            listOf(Employee("steeve")),
            listOf(Contractor("peter"))
        )
        val board = Department("board", listOf(Employee("boss"), Employee("partner")))
        company = Company(listOf(accountability, hr, board))
    }

    @Test
    fun `should return the number of employees in a department for accountability`() {
        val sut = CompanyManager(company)
        sut.countAccountability("accountability") shouldBe 3
        sut.countAccountability("hr") shouldBe 2
    }

    @Test
    fun `should return the number of employees in a department for HR`() {
        val sut = CompanyManager(company)
        sut.countHr("accountability") shouldBe 3
        sut.countHr("hr") shouldBe 1
    }

    @Test
    fun `should return the number of employees in a department for managers`() {
        val sut = CompanyManager(company)
        sut.countManager("accountability") shouldBe 2
        sut.countManager("hr") shouldBe 2
    }
}
