package z_various

import org.junit.jupiter.api.Test
import kotlin.random.Random

internal class PrintersTest {
    @Test
    fun printable() {

        val manager = PrinterManager()
        with(manager) {
            start()
            Thread.sleep(400)
        }

        manager.threadA?.join()
        manager.threadB?.join()

    }
}
