package z_various

import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.booleans.shouldBeTrue
import org.junit.jupiter.api.Test

internal class RangedVersionTest {
    @Test
    fun `should detect out of range`() {
        (RangedVersion(2, 6) in RangedVersion(2, 1)..RangedVersion(1, 1)).shouldBeFalse()
    }

    @Test
    fun `should range for same major version`() {
        (RangedVersion(1, 2) in RangedVersion(1, 1)..RangedVersion(1, 4)).shouldBeTrue()
    }

    @Test
    fun `should range for one major version above`() {
        (RangedVersion(1, 2) in RangedVersion(1, 1)..RangedVersion(2, 0)).shouldBeTrue()
    }

    @Test
    fun `should range for two major versions space`() {
        (RangedVersion(1, 2) in RangedVersion(0, 1)..RangedVersion(2, 0)).shouldBeTrue()
    }

    @Test
    fun `should range for several major versions space`() {
        (RangedVersion(3, 2) in RangedVersion(0, 1)..RangedVersion(7, 6)).shouldBeTrue()
    }
}
